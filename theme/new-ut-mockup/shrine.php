<?php if ( !defined( 'IN_GS' ) ) { die( 'you cannot load this page directly.' ); }?>

<?php include_once "inc/bootstrap.php"; global $FNS; ?>
<?php $FNS->inc("base/document-top.php") ?>
<?php $FNS->inc("header.php") ?>

<!-- Global Container -->
<div id="global_container">
  <div class="clear"></div>
  <?php $FNS->inc("banner.php") ?>
    <?php $FNS->inc("overnavi.php") ?>
   <!-- Content -->
   <div id="content" class="container_12">
      <?php $FNS->inc("breadcrumbs.php") ?>
      <?php $FNS->inc("base/sidebar.php") ?>
        <div class="grid_9">
            <h1>This is a test for all Things</h1>
            <img src="http://dummyimage.com/720x290/333333/dddddd" alt="">
            <?php get_page_content(); ?>
        </div>
      <!-- /Post -->    

   </div>
   <!-- /Content -->
  <div class="clear"></div>

</div>
<!-- /Global Container -->

<?php $FNS->inc("footer.php") ?>
<?php $FNS->inc("base/document-bottom.php") ?>
