<?php if(!defined('IN_GS')){ die('you cannot load this page directly.'); }?>

<?php include_once("inc/base/document-top.php") ?>

	<?php include_once("inc/header.php"); ?>
	<?php include_once("inc/single-column.php"); ?>
	<?php include_once("inc/footer.php"); ?>

<?php include_once("inc/base/document-bottom.php") ?>