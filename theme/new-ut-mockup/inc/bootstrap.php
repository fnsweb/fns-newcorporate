<?php
	/**
	 *  This file contains constants and utilities 
	 *  that will be used in all of the template.
	 *  
	 *  @author David Nuon 
	 *  @package FNS
	 *  @uses FNS\Utility
	 */

	// Get the FNS Utility Class
	require("FNSUtility.php");
	require("FNSModules.php");

	// Make a new instance of it, the first parameter
	// is the directory to where the PHP includes are
	$FNS = new FNS\Utility(dirname(__FILE__) . '/template');
?>