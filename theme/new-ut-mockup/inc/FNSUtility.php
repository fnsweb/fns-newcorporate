<?php
	namespace FNS;

	/**
	 *  FNSUtility 
	 *  Contains a collection of functions to make dealing with php easier
	 * 
	 *  @author David Nuon
	 */
	class Utility
	{
		/**
		 *  Default Constructor 
		 *  @param String $includePath - Path for base includes 
		 */
		function __construct($includePath) 
		{
			$this->includePath = $includePath;
		}

		/**
		 *	getIncludePath
		 *  @return String - Include specified in this instance
		 */
		public function getIncludePath()
		{
			return $this->includePath;
		}

		/**
	     * inc
		 * Does an include for a php file that exists in the specified base
		 * path
		 * 
		 * @uses include 
		 */
		public function inc($file) 
		{	
			include($this->includePath . '/' . $file);
		}
	}