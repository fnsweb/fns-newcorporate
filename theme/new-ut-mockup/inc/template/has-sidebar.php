<?php if ( !defined( 'IN_GS' ) ) { die( 'you cannot load this page directly.' ); }?>

<?php include_once "inc/bootstrap.php"; global $FNS; ?>
<?php $FNS->inc("base/document-top.php") ?>
<?php $FNS->inc("header.php") ?>

<!-- Global Container -->
<div id="global_container">
  <div class="clear"></div>
  <?php $FNS->inc("banner.php") ?>
   <!-- Content -->
   <div id="content" class="container_12">
      <?php $FNS->inc("breadcrumbs.php") ?>
      <?php $FNS->inc("base/sidebar.php") ?>
      <!-- Post -->
        <div id="post" class="grid_9">  
          <?php get_page_content(); ?>
        </div>
      <!-- /Post -->    
   </div>
   <!-- /Content -->
  <div class="clear"></div>
</div>
<!-- /Global Container -->

<?php $FNS->inc("footer.php") ?>
<?php $FNS->inc("base/document-bottom.php") ?>
