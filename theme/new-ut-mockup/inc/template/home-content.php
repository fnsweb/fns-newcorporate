<!-- ******************* START GLOBAL CONTAINER ******************* -->
<div id="global_container"> 
  <!-- START BRANDING -->
  <?php include_once("subnavi.php"); ?>
  <!-- END BRANDING -->
  <div class="clear"></div>
  <!-- START CONTENT -->
  <div id="content" class="container_12"> 
  
   <div id="intro" class="grid_12">
          <div class="grid_8 alpha">
              <h1 style="font-size:3em;letter-spacing:-1px;">CSULB Grid System</h1>
              <p style="font-size:1.8em; font-style:italic;color:#888;">The goal is to provide an invisible framework that allows content creators the flexibility to achieve any layout variation while retaining branding consistency. A grid system levels the playing field by solving common problems.</p>
              
              <p>The University Template has provided us with stability and brand consistency, it has enabled users to hit the ground running and for the most part, it has addressed the president's mandate to visually unify our university's online presence. </p>
             
             <h2>Moving Forward, Finally.</h2>
              <p>Branding remains consistent to what we have today. This includes the header and footer areas, however, all aspects of the layout are affected by the grid, either the 12, 16 or 24 column grids. The purpose of this new approach is to build upon what we have, maintain stability and consistency.</p>
          </div>


         <div class="grid_4 omega">
             <div class="graybox">
              <h2>Connect with Us</h2>
              <p>University Web Team<br>
                1000 Studebaker Rd,<br>
                Long Beach, CA 90815</p>
              <p><a href="mailto:webteam@csulb.edu" onclick="window.open('https://mail.google.com/mail/?view=cm&amp;tf=1&amp;to=webteam@csulb.edu&amp;cc=&amp;bcc=&amp;su=&amp;body=','_blank');return false;">webteam@csulb.edu</a></p>
              </div>

           <h2 style="text-align:center;font-size:3em;margin-bottom:1em;line-height:3em;">Examples</h2>
          </div>
  </div>
             
  <div class="clear"></div> 



  <div id="post" class="container_12">
    <div class="grid_12">
      <?php get_page_content(); ?>
    </div>
    <div class="clear"><br><br><br></div>
  </div>
      <!-- END POST --> 
      
  <!-- END CONTENT -->
  <div class="clear"></div>
  </div>   

</div>
<!-- ******************* END GLOBAL CONTAINER ******************* -->  
