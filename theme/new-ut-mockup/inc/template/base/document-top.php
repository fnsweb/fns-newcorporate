<!DOCTYPE HTML>
<html dir="ltr" lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
<title><?php get_page_title(); ?></title>

<?php // Stylesheets from the university ?>
<link rel="stylesheet" href="<?php get_theme_url(); ?>/css/reset.css" type="text/css" />
<link rel="stylesheet" href="<?php get_theme_url(); ?>/css/grid.css" type="text/css" />
<link rel="stylesheet" href="<?php get_theme_url(); ?>/css/grid_16.css" type="text/css" />
<link rel="stylesheet" href="<?php get_theme_url(); ?>/css/style.css" type="text/css" />

<?php // FNS Stuff ?>
<link rel="stylesheet" href="<?php get_theme_url(); ?>/css/structure.css" type="text/css" />
<link rel="stylesheet" href="<?php get_theme_url(); ?>/css/custom.css" type="text/css" />


<script type="text/javascript" src="<?php get_theme_url(); ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php get_theme_url(); ?>/js/util.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	 // hides the slickbox as soon as the DOM is ready
	  $('#destinations').hide();
	 // toggles the slickbox on clicking the noted link å 
	  $('#dest-toggle').click(function() {
	    $('#destinations').slideToggle(400);
	    return false;
	  });  
	});
</script>

<?php get_header(); ?>
</head>
<body>
