  <!-- START DESTINATIONS -->
  <div id="destinations" class="grid_12">  
  	<div class="grid_2 alpha">
      <ul>
        <li><a href="homepage.html">Homepage</a></li>
        <li><a href="navsidebar.html">Nav, Sidebar</a></li>
        <li><a href="sidebar.html">Sidebar</a></li>
        <li><a href="2sidebars.html">2 Sidebars</a></li>
        <li><a href="3sidebars.html">3 Sidebars</a></li>
        <li><a href="nosidebars.html">No Sidebars</a></li>
      </ul>
    </div>
    <div class="grid_1">
    <img src="<?php get_theme_url(); ?>/img/300w.jpg" />
      <ul>
        <li><a href="homepage.html">Homepage</a></li>
        <li><a href="navsidebar.html">Sidebar</a></li>
        <li><a href="sidebar.html">Sidebar</a></li>
      </ul>
    </div>
    <div class="grid_2">
      <ul>
        <li><a href="homepage.html">Homepage</a></li>
        <li><a href="navsidebar.html">Nav, Sidebar</a></li>
        <li><a href="sidebar.html">Sidebar</a></li>
        <li><a href="2sidebars.html">2 Sidebars</a></li>
        <li><a href="3sidebars.html">3 Sidebars</a></li>
        <li><a href="nosidebars.html">No Sidebars</a></li>
      </ul>
    </div>
    <div class="grid_3">
    <p>This is a sample image inside the new area called destinations</p>
      <ul>
        <li><a href="homepage.html">Homepage</a></li>
        <li><a href="navsidebar.html">Nav, Sidebar</a></li>
        <li><a href="sidebar.html">Sidebar</a></li>
      </ul>
    </div>
    <div class="grid_3">
      <img src="<?php get_theme_url(); ?>/img/700x200.png" />
      <p>This is a sample image inside the new area called destinations</p>
    </div>
    <div class="grid_1 omega">
    <img src="<?php get_theme_url(); ?>/img/300w.jpg" />
      <ul>
        <li><a href="homepage.html">Homepage</a></li>
        <li><a href="navsidebar.html">Sidebar</a></li>
        <li><a href="sidebar.html">Sidebar</a></li>
      </ul>
    </div> 
  </div> 
  <!-- END DESTINATIONS --> 