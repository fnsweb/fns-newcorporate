<!-- START BRANDING -->
<div class="banner-navi">
	<div class="container_12 print-hide">
		<div class="grid_4">
			<br>
			<img src="<?php get_theme_url(); ?>/img/logo.png" alt="">
		</div>
		<nav class="grid_8">
			<ul class="horizontal-navi">
				<li class="navi-item"><a href="#">Bookstore</a></li>
				<li class="navi-item"><a href="#">Corporate</a></li>
				<li class="navi-item"><a href="#">Beach Card</a></li>
				<li class="navi-item"><a href="#">Dining Services</a></li>
				<li class="navi-item"><a href="#">Other Stores</a></li>
			</ul>
		</nav>
	</div>
</div>
<!-- END BRANDING -->