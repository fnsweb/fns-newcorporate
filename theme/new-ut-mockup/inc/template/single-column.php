<?php if(!defined('IN_GS')){ die('you cannot load this page directly.'); }?>

<!-- ******************* START GLOBAL CONTAINER ******************* -->
<div id="global_container"> 

  <?php include_once("branding.php"); ?>
  <?php include_once("subnavi.php"); ?>

  <div class="clear"></div>
  <div id="content" class="container_12"> 
      <?php include_once('breadcrumbs.php'); ?>
      <!-- START POST -->
      <div id="post" class="grid_12">
      	<h1><?php get_page_title(); ?></h1>
				<?php get_page_content(); ?>
      </div>
      <!-- END POST --> 
      
   </div>
  <!-- END CONTENT -->
  <div class="clear"></div>
</div>
  <!-- END CONTENT -->
  <div class="clear"></div>
</div>

<!-- ******************* END GLOBAL CONTAINER ******************* -->  

