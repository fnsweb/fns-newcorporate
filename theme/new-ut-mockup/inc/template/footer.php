
  <!-- START FOOTER -->
  <div id="footer" class="container_12">     
    <!-- START INDEX -->
    <div id="index" class="grid_12">
      <h2><a href="#">CSULB Website Index</a></h2>
      <ul>
        <li><a href="/sitemap/">A</a></li>
        <li><a href="/sitemap/alphapages/B.html">B</a></li>
        <li><a href="/sitemap/alphapages/CA-CL.html">CA-CL</a></li>
        <li><a href="/sitemap/alphapages/CM-CZ.html">CM-CZ</a></li>
        <li><a href="/sitemap/alphapages/D.html">D</a></li>
        <li><a href="/sitemap/alphapages/E.html">E</a></li>
        <li><a href="/sitemap/alphapages/F.html">F</a></li>
        <li><a href="/sitemap/alphapages/G.html">G</a></li>
        <li><a href="/sitemap/alphapages/H.html">H</a></li>
        <li><a href="/sitemap/alphapages/I.html">I</a></li>
        <li><a href="/sitemap/alphapages/J.html">J</a></li>
        <li><a href="/sitemap/alphapages/K.html">K</a></li>
        <li><a href="/sitemap/alphapages/L.html">L</a></li>
        <li><a href="/sitemap/alphapages/M.html">M</a></li>
        <li><a href="/sitemap/alphapages/N.html">N</a></li>
        <li><a href="/sitemap/alphapages/O.html">O</a></li>
        <li><a href="/sitemap/alphapages/P.html">P</a></li>
        <li><a href="/sitemap/alphapages/Q.html">Q</a></li>
        <li><a href="/sitemap/alphapages/R.html">R</a></li>
        <li><a href="/sitemap/alphapages/S.html">S</a></li>
        <li><a href="/sitemap/alphapages/T.html">T</a></li>
        <li><a href="/sitemap/alphapages/U.html">U</a></li>
        <li><a href="/sitemap/alphapages/V.html">V</a></li>
        <li><a href="/sitemap/alphapages/W.html">W</a></li>
        <li><a href="/sitemap/alphapages/XYZ.html">X Y Z</a></li>
      </ul>
    </div>
    <!-- END INDEX -->
    <!-- START FOOTER COLUMNS -->
    <div id="footer_a" class="grid_4 ">
      <p>California State University, Long Beach<br />
        1250 Bellflower Blvd, Long Beach, California 90840<br />
        562.985.4111</p>
    </div>
    <div id="footer_b" class="grid_2 ">
      <ul>
        <li><a href="homepage.html">Homepage</a></li>
        <li><a href="navsidebar.html">Nav, Sidebar</a></li>
        <li><a href="sidebar.html">Sidebar</a></li>
        <li><a href="2sidebars.html">2 Sidebars</a></li>
        <li><a href="3sidebars.html">3 Sidebars</a></li>
        <li><a href="nosidebars.html">No Sidebars</a></li>
      	</ul>
    </div>
    <div id="footer_c" class="grid_2">
      <ul>
        <li>Website Credits</li>
        <li>Feedback</li>
        <li>Help</li>
      </ul>
    </div>
    <div id="footer_d" class="grid_2 ">
      <ul>
        <li>Website Credits</li>
        <li>Feedback</li>
        <li>Help</li>
      </ul>
    </div>
    <div id="footer_e" class="grid_2 ">
      <ul>
        <li>Website Credits</li>
        <li>Feedback</li>
        <li>Help</li>
      </ul>
    </div>
    <!-- END FOOTER COLUMNS -->
  </div>
  <!-- END FOOTER --> 