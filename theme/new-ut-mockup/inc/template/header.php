<?php
  /*
    The Header of the template

  */

?>

<div id="header" class="container_12"> 
  
  <!-- START LOGO -->
  <div id="logo" class="grid_3">
  <a href="#"><img src="<?php get_theme_url(); ?>/img/csulbseal.gif" alt="CSULB" title="CSULB" /></a>
  </div>
  <!-- END LOGO --> 
  
   
    <!-- START SEARCHBOX -->
    <div id="toolsearchset" class="grid_4 push_4">
      <input name="q" type="text" class="toolsearchbox" id="toolsearchbox" accesskey="s" title="Search Box" value="">
      <input name="toolsearchbox" type="submit" class="toolsearchbutton" id="toolsearchbutton" title="Search" value="Go">
    </div>
    <div class="grid_1 push_4"><img id="dest-toggle" src="<?php get_theme_url(); ?>/img/expand.gif" /></div>     
    <!-- END SEARCHBOX -->

  <?php include_once("tools.php"); ?>
  <?php include_once("destinations.php"); ?>
</div>
<!-- END HEADER -->

<div class="clear"></div>