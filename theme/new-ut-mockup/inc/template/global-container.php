<!-- ******************* START GLOBAL CONTAINER ******************* -->
<div id="global_container"> 
  <!-- START BRANDING -->
  <div id="branding" class="container_12">
    <div class="grid_4">&nbsp;</div>
    <div class="grid_8">
    <div id="topnav">
      <ul style="float:right;">
        <li class="current"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Sites</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
    </div>
    </div>
  </div>
  <!-- END BRANDING -->
  <div class="clear"></div>
  <!-- START CONTENT -->
  <div id="content" class="container_12"> 
    
      <!-- START NAV -->
    	<!--
<div id="nav" class="grid_3">
      	<ul>
        <li><a href="homepage.html">Homepage</a></li>
        <li><a href="navsidebar.html">Nav, Sidebar</a></li>
        <li><a href="sidebar.html">Sidebar</a></li>
        <li><a href="2sidebars.html">2 Sidebars</a></li>
        <li><a href="3sidebars.html">3 Sidebars</a></li>
        <li><a href="nosidebars.html">No Sidebars</a></li>
      	</ul>
    	</div>
-->
      <!-- END NAV --> 
      
      <!-- START FEATURE -->
      <!--
<div id="feature" class="grid_12">
      <img src="img/700x200.png" width="940" />
      </div> 
-->
      <!-- END FEATURE --> 
      
      <!-- START BREADCRUMBS -->
      <!--
<div id="breadcrumbs" class="grid_12">
      <a href="#">Home</a> &rarr; Breadcrumb &rarr; Breadcrumb &rarr; Breadcrumb
      </div>
      <div class="clear"></div> 
-->
      <!-- END BREADCRUMBS --> 
      
      <!-- START POST -->
      <div id="intro" class="grid_12">
      <div class="grid_8 alpha">
      <h1 style="font-size:3em;letter-spacing:-1px;">CSULB Grid System</h1>
      <p style="font-size:1.8em; font-style:italic;color:#888;">The goal is to provide an invisible framework that allows content creators the flexibility to achieve any layout variation while retaining branding consistency. A grid system levels the playing field by solving common problems.</p>
      
      <p>The University Template has provided us with stability and brand consistency, it has enabled users to hit the ground running and for the most part, it has addressed the president's mandate to visually unify our university's online presence. </p>
     
     <h2>Moving Forward, Finally.</h2>
	<p>Branding remains consistent to what we have today. This includes the header and footer areas, however, all aspects of the layout are affected by the grid, either the 12, 16 or 24 column grids. The purpose of this new approach is to build upon what we have, maintain stability and consistency.</p>
	
	
	
      </div>
      <div class="grid_4 omega">
     <div class="graybox">
      <h2>Connect with Us</h2>
      <p>University Web Team<br />
        1000 Studebaker Rd,<br />
        Long Beach, CA 90815</p>
      <p><a href="mailto:webteam@csulb.edu">webteam@csulb.edu</a></p>
      </div>

      </div>
       <h2 style="text-align:center;font-size:3em;margin-bottom:1em;line-height:3em;">Examples</h2>
      </div>
      
      <div class="clear"></div> 
      
      <div id="post" class="container_16">
                
        <div class="grid_4">
        <h3>Default Layout</h3>
        <a href="public/navsidebar.html"><img src="img/1.gif" /></a>
        <p>This layout resembles the current University template with its navigation and right column areas</p>
        </div>
        
        <div class="grid_4">
        <h3>More Columns</h3>
        <a href="public/2sidebars.html"><img src="img/2.gif" /></a>
        <p>Left side navigation absent, one more column added on the right hand side</p>
        </div>
        
        <div class="grid_4">
        <h3>Content Only</h3>
        <a href="public/nosidebars.html"><img src="img/4.gif" /></a>
        <p>Displaying only the content having removed the navigation, sidebars and any other content element</p>
        </div>
        
        <div class="grid_4">
        <h3>Really Busy</h3>
        <a href="public/homepage.html"><img src="img/4b.gif" /></a>
        <p>If needed, this example illustrates how complex of a layout can be achieved using a 12 column grid</p>
        </div>
        
        
        
        <div class="clear"></div>
        
        <h2 style="text-align:center;font-size:3em;margin-bottom:1em;line-height:3em;">More Possibilities...</h2>
        
        
        <div class="grid_4">
        <h3>Black Background</h3>
        <a href="public/homepage-black.html"><img src="img/5.gif" /></a>
        <p>Using a 12 column grid and a black background approach</p>
        </div>
        
        <div class="grid_4">
        <h3>Using 16 Column Grid</h3>
        <a href="public/homepage-black-news.html"><img src="img/6.gif" /></a>
        <p>Using a 16 column grid and a black background approach</p>
        </div>
        
        <div class="grid_4">
        <h3>Other Ideas</h3>
        <a href="public/metro.html"><img src="img/7.gif" /></a>
        <p>Illustrating the flexibility of this framework</p>
        </div>
        
        <div class="grid_4">
        <h3>Using White Background</h3>
        <a href="public/news4.html"><img src="img/8.gif" /></a>
        <p></p>
        </div>
        
        <div class="clear"></div>
                
        <h2 style="text-align:center;font-size:3em;margin-bottom:1em;line-height:3em;">Links</h2>
        
        <div class="grid_4">
        <h2>The Current University Template</h2>
        <p><a href="/ut">The "UT"</a></p>
        </div>
        
        <div class="grid_4">
        <h2>The Grid</h2>
        <p><a href="http://960.gs">960.gs</a> Grid System Website</p>
        </div>
        
        <div class="grid_4">
        <h2>Grid Overlay Tool</h2>
        <p>Download the <a href="http://peol.github.com/960gridder/">Grid overlay Tool</a></p>
        </div>
        
         <div class="grid_4">
        </div>
        
      </div>

      <!-- END POST --> 
      
   </div>
  <!-- END CONTENT -->
  <div class="clear"></div>
</div>
<!-- ******************* END GLOBAL CONTAINER ******************* -->  

