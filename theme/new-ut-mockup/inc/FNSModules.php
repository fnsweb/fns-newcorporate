<?php
	namespace FNS\Modules;

	/**
	 *  Hero 
	 *  Class that represents a hero
	 * 
	 *  @author David Nuon
	 */
	class Hero
	{
		/**
		 *  Default Constructor 
		 *  @param String $includePath - Path for base includes 
		 */
		function __construct($name, $headline, $image, $isBigHero, $subHeadLine = NULL, $links = NULL) 
		{
			$this->name = $name;
			$this->headline = $headline;
			$this->image = $image;
			$this->isBigHero = $isBigHero;
			$this->subHeadLine = $subHeadLine;
			$this->links = $links;
		}

		/**
		 *	render
		 *  @return String - markup for the hero
		 */
		public function render()
		{
			// TODO: Implement this.
			return '';
		}

	}