<?php if ( !defined( 'IN_GS' ) ) { die( 'you cannot load this page directly.' ); }?>

<?php include_once "inc/bootstrap.php"; global $FNS; ?>
<?php $FNS->inc("base/document-top.php") ?>
<?php $FNS->inc("header.php") ?>

<!-- Global Container -->
<div id="global_container">
	
	<?php $FNS->inc("branding.php"); ?>

	<!-- Masthead -->
	<div id="masthead">
		<div class="over-navi" id="main-navi">
		  <nav>
		  	<ul>
			    <li class="bookstore"><a href="#">Bookstore</a></li>
			    <li class="dining"><a href="#">Dining Services</a></li>
			    <li class="other"><a href="#">Other Stores</a></li>
			    <li class="beachcard"><a href="#">ID Card Services</a></li>
		    </ul>
		  <?php $FNS->inc("dropdown.php"); ?>
		  </nav>
		</div>
	  <img style="width:100%" src="<?php get_theme_url(); ?>/img/hero.jpg" alt="">
	</div> 
	<!-- /Masthead -->

	<div class="clear"></div>

	<!-- Little Heroes -->
	<div class="container_12">
		<div class="grid_6 corporate front-page-tab">
			<div class="info">
				<h2>About Us</h2>
				<ul>
					<li><a href="sustainability">Sustainability</a></li>
					<li><a href="corporate-information">Corporate Information</a></li>
					<li><a href="mission-statement">Mission</a></li>
					<li><a href="employment">Employment</a></li>
				</ul>
			</div>
		</div>

		<div class="grid_6 beachbreeze front-page-tab">
			<div class="info">
				<h2>BeachBreeze</h2>
				<ul>
					<li><a href="#">Winter 2012</a></li>
					<li><a href="#">Winter 2013</a></li>
					<li><a href="#">Summer 2011</a></li>
				</ul>
			</div>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
	<!-- /Little Heroes -->

	<script>
	$(function () {
	    $("#dropdown").hide();
	    
	    $("nav ul li").mouseover(
	      function() {
	        var bgImage = $(this).attr('class');
	        $("nav ul li").removeClass('selected');
	        $("#dropdown").attr('class', bgImage);
	        $('.dropdown-content').hide();
	        $('.dropdown-content.' + bgImage).show();
	        $(this).addClass('selected');
	      }
	    );
	    $("nav").hover(
	      function () {
	         $("#dropdown").show();
	      },
	      function () {
	         $("#dropdown").hide();
	         $("#main-navi ul li").removeClass('selected');
	      }
	    );
	  });
	</script>	


</div>
<!-- /Global Container -->

<?php $FNS->inc("footer.php") ?>
<?php $FNS->inc("base/document-bottom.php") ?>
