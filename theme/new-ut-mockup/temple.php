<?php if ( !defined( 'IN_GS' ) ) { die( 'you cannot load this page directly.' ); }?>

<?php include_once "inc/bootstrap.php"; global $FNS; ?>
<?php $FNS->inc("base/document-top.php") ?>
<?php $FNS->inc("header.php") ?>

<!-- Global Container -->
<div id="global_container">
  <div class="clear"></div>
  <?php $FNS->inc("banner.php") ?>
   <!-- Content -->
   <div id="content" class="container_12">
      <?php $FNS->inc("breadcrumbs.php") ?>
      
      <!-- Post -->
        <div  class="grid_12">  
            <img src="http://dummyimage.com/940x390/333333/dddddd" alt="">
        </div>
      
        <div class="grid_3" style="text-align:center;"><img src="http://dummyimage.com/200x100/333333/dddddd" alt=""></div>
        <div class="grid_3" style="text-align:center;"><img src="http://dummyimage.com/200x100/333333/dddddd" alt=""></div>
        <div class="grid_3" style="text-align:center;"><img src="http://dummyimage.com/200x100/333333/dddddd" alt=""></div>
        <div class="grid_3" style="text-align:center;"><img src="http://dummyimage.com/200x100/333333/dddddd" alt=""></div>
       
        <div class="grid_12"><br></div>
        <?php $FNS->inc('base/sidebar.php'); ?>
        
        <div class="grid_9">
                    <?php get_page_content(); ?>

        </div>
      <!-- /Post -->    
   </div>
   <!-- /Content -->
  <div class="clear"></div>
</div>
<!-- /Global Container -->

<?php $FNS->inc("footer.php") ?>
<?php $FNS->inc("base/document-bottom.php") ?>
