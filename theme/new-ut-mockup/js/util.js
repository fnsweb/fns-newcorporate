function breadCrumbs(id) {
	console.log(id);
	var $elem = $(id);
	
	// The breakcrumb based on the url
	var path = document.URL.split('/').filter( function(e) { 
			
			return  e.length > 0       // We want non-empty strings
					&& e != "http:"    // We also don't want the protocol part.
					&& e != "https:";

	});

	var out = [];

	for (var i = path.length - 1; i >= 0; i--) {
		var part = "" 
		part += '<a href="';
		part += "http://" + path.slice(0, 1 + i).join("/");
		part += '">';
		part += path[i];
		part += "</a>";

		console.log(part);
		out[out.length] = part;
		console.log(out);
	};

	$elem.html(out.reverse().join("&nbsp;&rarr;&nbsp;"));
}